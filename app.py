import tkinter as tk
from tkinter import messagebox
from rooks_solver import RooksSolver
from functools import partial

class GameBoard(tk.Frame):
    def __init__(self, parent, rows, columns, size, color1, color2):
        '''config'''
        self.counter = 0
        self.rows = rows
        self.columns = columns
        self.size = size
        self.color1 = color1
        self.color2 = color2
        self.pieces = {}
        canvas_width = columns * size
        canvas_height = rows * size

        tk.Frame.__init__(self, parent)
        self.canvas = tk.Canvas(self, borderwidth=0, highlightthickness=0,
                                width=canvas_width, height=canvas_height, background="bisque")
        self.canvas.pack(side="top", fill="both", expand=True, padx=2, pady=2)
        self.imagedata = '''
        iVBORw0KGgoAAAANSUhEUgAAADAAAAAwCAYAAABXAvmHAAAABmJLR0QA/wD/AP+gvaeTAAACEUlEQVRoge2Yz0tVQRTHv2e894llPn9EYGmBqxaGgeBCozb6hIp2D7RWUryN/QEt+wfERTuR2oQI/gGKGrjwbVq5tG2Bq+Al9bB83hlXgsQbr575ggXzWV3ud858z2HO4TIXiEQikUgAErrBxPNX7uR5Y2lRmr0/Szv9XoMJCf4XiAVcNtQCJl68HAOA8enKg4toIVCHWEMc4stOIBRGAVV9qNsONQ+egdO4z3erAEZzllVlZJc2yOwWyh9oJ0FD/zfUEwCA4Uol7f5lD5tpjb2+dGvr7RHTjz7EN+r1Np/WMfDVq2mhF3BwmF7xafUD49W00AswpnDVpzlr2ul+7A2TluyWT0tTe5PtRy/ACvq8WubXtPC/xFZu+yQH3GHb8WdAcM8rCgbpfuwNHeS+TxPAq2mhfsjGy5WipPY7gMSz5Mg1zPXNlYV9lif1BKTVPYI/eQBITJI9ZHpyW8jhce4SY54wLWktVC6XCz/S4h6AnpyltaTW1ru6+u4Pw5d2AvuF4jPkJw8AXY3O309ZvrQCnMXsedcK3GuWb3ALnfMS4yP4csM4Af0FhXC5UZ9AaWqmH8bMW0hJgGuaPRzwE5BPicnerH18/0Wzh6qA0tRMvzMtOwC6NfFNqInNhtaXP3y7aKCuhYyZBy95AOhyJplTpaIJspCSJi6HSU2QqgBtz5+N69BE/fd/5iKRSCQSxDHxT3kSrbzUQAAAAABJRU5ErkJggg==
        '''
        self.rook = tk.PhotoImage(data=self.imagedata)
        self.buttonCalculate = tk.Button(self, text ="Calculate!", command = self.calculate)
        self.buttonCalculate.pack()
        self.buttonRefresh = tk.Button(self, text ="Refresh Board", command = self.clear)
        self.buttonRefresh.pack()
        self.canvas.bind("<Configure>", self.draw)
        self.canvas.bind("<Button-1>", self.callback)
    def callback(self, event):
        if self.counter <= (self.rows - 1):
            self.placepiece(str(self.counter),event.x, event.y)
        else:
            messagebox.showinfo( "Info", "Maximum input reached!")
        print("clicked at", event.x, event.y)
    
    def clear(self):
        self.canvas.delete("all")
        self.pieces.clear()
        self.counter = 0
        color = self.color2
        for row in range(self.rows):
            if self.rows % 2 == 0:
                color = self.color1 if color == self.color2 else self.color2
            for col in range(self.columns):
                x1 = (col * self.size)
                y1 = (row * self.size)
                x2 = x1 + self.size
                y2 = y1 + self.size
                self.canvas.create_rectangle(x1, y1, x2, y2, outline="black", fill=color, tags="square")
                color = self.color1 if color == self.color2 else self.color2
        self.canvas.tag_raise("piece")
        self.canvas.tag_lower("square")
    
    def calculate(self):
        rooks_array = []
        message = "Tidak Satisfiable"
        if len(self.pieces) < self.rows:
            messagebox.showinfo("Error", "Must have " + str(self.rows) + " Rooks")
            return
        for value in self.pieces.values():
            position = value[0] + value[1]*self.rows + 1
            rooks_array.append(position)
        rs = RooksSolver()
        print('ROOKS ARRAY')
        print(rooks_array)
        rs.set_rooks(self.rows, rooks_array)
        if rs.is_satisfied():
            message = "Satisfiable"
        messagebox.showinfo("Result", message)


    def placepiece(self, name, row, column):
        '''Place a piece at the given row/column'''
        c = (column) // self.size
        r = (row) // self.size

        x0 = (c * self.size) + int(self.size/2)
        y0 = (r * self.size) + int(self.size/2)
        print(self.size)
        if (r,c) in self.pieces.values():
            print("exist")
            print(self.pieces)
        else:
            self.pieces[name] = (r, c)
            self.placed = name
            print("placed at", x0, y0)
            print(self.pieces)
            self.counter += 1
            self.canvas.create_image(y0,x0, image=self.rook, tags=(name, "piece"), anchor="c")

    def draw(self,event):
        '''Redraw the board, possibly in response to window being resized'''
        color = self.color2
        for row in range(self.rows):
            if self.rows % 2 == 0:
                color = self.color1 if color == self.color2 else self.color2
            for col in range(self.columns):
                x1 = (col * self.size)
                y1 = (row * self.size)
                x2 = x1 + self.size
                y2 = y1 + self.size
                self.canvas.create_rectangle(x1, y1, x2, y2, outline="black", fill=color, tags="square")
                color = self.color1 if color == self.color2 else self.color2
        for name in self.pieces:
            self.placepiece(name, self.pieces[name][0], self.pieces[name][1])
        self.canvas.tag_raise("piece")
        self.canvas.tag_lower("square")


# image comes from the silk icon set which is under a Creative Commons
# license. For more information see http://www.famfamfam.com/lab/icons/silk/


def run():
    size = int(var.get())
    rootBoard = tk.Toplevel(root)
    rootBoard.title("N-Rooks Satisfiability")
    rootBoard.resizable(0, 0)
    board_size = 512/size
    board = GameBoard(rootBoard, size, size, board_size, "white", "black")
    board.pack(side=tk.TOP, fill="both", expand=1, padx=8, pady=8)
    rootBoard.mainloop()
    
if __name__ == "__main__":
    root = tk.Tk()
    root.title("N-Rooks Satisfiability")
    # root.resizable(0, 0)
    row = tk.Frame(root)
    row.grid(column=0,row=0)
    row.columnconfigure(0, weight = 1)
    row.rowconfigure(0, weight = 1)
    lab = tk.Label(row, width=15, text='N', anchor='w')
    var = tk.StringVar(root)

    var.set("2") # set the default option
    inp = tk.OptionMenu(row, var,"2", "4", "8", "10", "12")
    row.pack(side=tk.TOP, fill=tk.X, padx=50, pady=50)
    lab.pack(side=tk.LEFT)
    inp.pack(side=tk.RIGHT, expand=tk.YES, fill=tk.X)
    run = tk.Button(root, text='Generate',
                  command= run)
    run.pack(side=tk.RIGHT, padx=105, pady=5)
    root.mainloop()